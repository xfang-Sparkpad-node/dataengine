const Basichttp = require('basichttp');

module.exports = class DataEngine extends Basichttp {
  constructor (host, port, protocol, username, password) {
    super();
    this.host = host;
    this.port = port;
    this.protocol = protocol;
    this.username = username;
    this.password = password;
    this.auth = 'Basic ' +  new Buffer(this.username + ':' + this.password).toString('base64');
  }

  /**
   * Update Auth
   * @param {String <Token type>} token_type
   * @param {String <Acount Token>} value
   */
  updateAuth (token_type, value) {
    this.auth = token_type + ' ' + value;
  }

  /**
   * group business
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  business ({include = undefined, filter = undefined, fields = undefined}) {
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let path = '/jsonapi/group/business';
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * group store
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  store ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/group_content/business-group_store-online';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * group product
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  product ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/group_content/business-group_product-default';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * group vocabulary
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  vocabulary ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/group_content/business-group_vocabulary';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * group vocabulary
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  taxonomy ({include = undefined, filter = undefined, fields = undefined, path = undefined}) {
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * group user
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  user ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/user/user';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * business-group_promotion
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  promotion ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/group_content/business-group_promotion';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * business-group_membership
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  membership ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/group_content/business-group_membership';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * business-product_bundle-default
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  product_bundle ({include = undefined, filter = undefined, fields = undefined}) {
    let path = '/jsonapi/group_content/business-product_bundle-default';
    let that = this;
    let options = {
      host: that.host,
      method: 'GET',
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }

  /**
   * node--device
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  device ({include = undefined, filter = undefined, fields = undefined, data = '', method = 'GET', id = ''}) {
    let path = '/jsonapi/node/device' + id;
    let that = this;
    let options = {
      host: that.host,
      method: method,
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port,
      data: data
    };
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }
};